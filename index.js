var fs = require('fs');
var util = require('util');
var async = require('async');
var path = require('path');


exports.Itinerary = require('./common/itinerary');
exports.TransformService = require('./common/transformservice');
exports.JsonMap = require('./common/jsonmap');
exports.SqsClientProvider = require('./common/sqsclientprovider');

exports.OnRamps = {};

exports.OnRamps.ExcelOnRamp = require('./onramps/excelonramp');
exports.OnRamps.FileOnRamp = require('./onramps/fileonramp');
exports.OnRamps.SqsOnRamp = require('./onramps/sqsonramp');

exports.OffRamps = {};

exports.OffRamps.DebugOffRamp = require('./offramps/debugofframp');
exports.OffRamps.HttpOffRamp = require('./offramps/httpofframp');
exports.OffRamps.HttpScraper = require('./offramps/httpscraper');
exports.OffRamps.SqsOffRamp = require('./offramps/sqsofframp');
exports.OffRamps.FileOffRamp = require('./offramps/fileofframp');




exports.start = function() {

		var bus = require('simplebus').createBus();

		var itineraries = [];

		util.log("Starting ESB...");

		process.on('uncaughtException', function (err) {
			util.log('Uncaught exception.');
  			util.error(err);  			
		});


		// load itineraries
		fs.readdirSync("./config/itineraries").forEach(function(file) {
		  util.log('loading itinerary [' + file + ']...');
		  var location = path.resolve("./config/itineraries/" + file);
		  var i = require(location).itinerary;
		  i.Name(file);

		  itineraries.push(i);
		});

		util.log('loaded ' + itineraries.length + ' itineraries');

		// start itineraries
		async.each(itineraries, function(item, done){
				util.log('starting itinerary [' + item.name + ']...');
				item.Configure(bus);
				item.Start();
				done();
			},
			function(err){
				if (err) {
					util.log('Failed to startup!')
					util.error(err);
				}

				util.log('Completed successful startup.')
			});

}