var util = require('util');

function JsonMap() {
	this.fields = [];
	this.collections = [];
}

JsonMap.prototype = {
		
		field: function(source, dest, resolver) {
			
			this.fields.push({ source: source, dest: dest, resolver: resolver });
			return this;
		},
		
		collection: function(source, dest, resolver) {
			this.collections.push({ source: source, dest: dest, resolver: resolver });
			return this;
		},
		
		apply: function(source) {
			
			var tmp_source = null;
			
			if (typeof source == 'string') {
				tmp_source = JSON.parse(source);
				
			} else {
				tmp_source = source;
			}
			
			var dest = new Object();			
			
			for (var i = 0; i < this.fields.length; i++) {
				
				var field = this.fields[i];
				
				util.log('Mapping field: ' + field.source + ' -> ' + field.dest);
				
				if (field.resolver instanceof JsonMap)
				{
					util.log('	submap -> ');
					dest[field.dest] = field.resolver.apply(this.valueFromPath(tmp_source, field.source));
					
				} else if (typeof field.resolver == 'function') {
					dest[field.dest] = field.resolver(this.valueFromPath(tmp_source, field.source));
					
				} else {
						dest[field.dest] = this.valueFromPath(tmp_source, field.source);
					}				
			}
			
			for (var i = 0; i < this.collections.length; i++) {
				
				var field = this.collections[i];
			
				
				util.log('Mapping collection: ' + field.source + ' -> ' + field.dest);
				
				dest[field.dest] = [];
				
				var coll = this.valueFromPath(tmp_source, field.source);
				
				for (var j = 0; j < coll.length; j++) {
				
					var item = coll[j];
					
					if (field.resolver instanceof JsonMap)
					{
						dest[field.dest].push(field.resolver.apply(item));
					}
				}				
			}			
			
			return dest;
		},
		
		valueFromPath: function (obj, path) {
			  
			  if (!path || path.length == 0) return null;
			  
			  if (path == '*') return obj;

			  var prop, props = path.split('.');

			  for (var i = 0, iLen = props.length - 1; i < iLen; i++) {
			    prop = props[i];

			    if (typeof obj == 'object' && obj !== null && prop in obj) {
			      obj = obj[prop];
			    } else {
			      break;
			    }
			  }
			  return obj[props[i]];
			}
};

module.exports = JsonMap;