var util = require('util');
var config = require('config');
var SQS = require('aws-sqs');

function SqsClientProvider(){
	this.sqs = null;
}

SqsClientProvider.prototype = {
	
		autoConfigure: function() {		
			
			if (config.AWSCredentials) {
				util.log('auto configuring SQS Provider using configuration...');
				this.sqs = new SQS(config.AWSCredentials.key, config.AWSCredentials.secret, {region: config.AWSCredentials.region });
			}
			
			return this;
		},
		
		configure: function(key, secret, region) {
			
			this.sqs = new SQS(key, secret, {region: region });
			return this;
		},
		
		getClient: function() {
		
			return this.sqs;
		}		
};

module.exports = new SqsClientProvider();