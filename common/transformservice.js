/*
 * The TransformService is responsible for applying message transformations.
 */
var BusService = require('./busservice');

function TransformService() {
	BusService.call(this);
	this.map = null;
	this.split = null;
}

TransformService.prototype = new BusService();
TransformService.prototype.constructor = TransformService;

TransformService.prototype.provider = function(jsonMap) {
			this.map = jsonMap;
			return this;
		};

TransformService.prototype.Split = function(fieldName) {
			this.split = fieldName;
			return this;
		};

		
TransformService.prototype.execute = function(msg) {
			var service = this;
			var newPayload = this.map.apply(msg.payload);			

			if (this.split) {
				newPayload[this.split].forEach(function(item){
					service.publish(item);
				});
			} else {
				this.publish(newPayload);	
			}			
		};
		
module.exports = TransformService;