var util = require('util');
/*
 * This is a base class for implementing bus services. You
 * should override the execute() method and call publish() with
 * your results.
 */
function BusService() {
	this.bus = null;
	this.stopped = true;
	this.subscriptionEventName = null;
	this.publicationEventName = null;
	this.map = null;
}

BusService.prototype = {
		
		configure: function(serviceBus) {			
			this.bus = serviceBus;
			return this;
		},

		subscribeTo: function(busEvent) {
			this.subscriptionEventName = busEvent;
			return this;
		},
		
		publishAs: function(busEvent) {
			this.publicationEventName = busEvent;
			return this;
		},
		
		start : function() {
			
			var exp = new RegExp('^'.concat(this.subscriptionEventName.replace('*', '.*'), '$'));
			
			var subscription = function(msg) {
				
				return msg.event.match(exp);
			};
			
			util.log('BusService -> start listening [' + this.subscriptionEventName + ']...');
			this.stopped = false;
			
			
			var self = this;
			
			this.bus.subscribe(
					subscription, 
					function(msg) { self.execute(msg); });
		},
		
		stop : function() {
			util.log('BusService -> shutting down...');
			this.stopped = true;
		},
		
		execute: function(msg) {},
		
		publish : function(msg){
			
			util.log('BusService -> publish');
			var evt = this.publicationEventName;
			
			var busMsg = { event: evt, payload: msg };
			util.log('BusService -> publishing message...');
			util.log(busMsg);
			this.bus.post(busMsg);
		}		
};

module.exports = BusService;