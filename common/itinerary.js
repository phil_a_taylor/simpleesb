var util = require('util');
var async = require('async');

function Itinerary(){
	this.bus = null;
	this.name = '';
	this.onramps = [];
	this.offramps = [];
	this.transforms = [];
}

Itinerary.prototype.Name = function(name) {
	this.name = name;
	return this;
}			

Itinerary.prototype.Configure = function(serviceBus) {			
	this.bus = serviceBus;

	var todo = this.onramps.concat(this.transforms, this.offramps);
	async.each(todo, function(item, done){
		item.configure(serviceBus);
		done();
	},
	function(err){
		if (err) util.error(err);

		return this;
	});
}

Itinerary.prototype.OnRamp = function(onramp){
	
	if (this.bus != null) onramp.configure(this.bus);

	this.onramps.push(onramp);
	return this;
}

Itinerary.prototype.OffRamp = function(offramp){
	
	if (this.bus != null) offramp.configure(this.bus);

	this.offramps.push(offramp);
	return this;
}

Itinerary.prototype.Transform = function(transform){

	if (this.bus != null) transform.configure(this.bus);

	this.transforms.push(transform);
	return this;
}

Itinerary.prototype.Start = function(){
	
	var toStart = this.onramps.concat(this.transforms, this.offramps);
	async.each(toStart, function(item, done){
		item.start();
		done();
	},
	function(err){
		if (err) util.error(err);
	});
}

Itinerary.prototype.Stop = function() {
	var toStop = this.onramps.concat(this.transforms, this.offramps);
	async.each(toStop, function(item, done){
		item.stop();
		done();
	},
	function(err){
		if (err) util.error(err);
	});
}

module.exports = Itinerary;