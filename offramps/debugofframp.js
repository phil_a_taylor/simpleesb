var util = require('util');

/*
 * Debug Off Ramp sends messages to the log file
 * remarks: currently only supports JSON object payloads
 */

var OffRamp = require('./offramp');

function DebugOffRamp() {	
}

DebugOffRamp.prototype = new OffRamp(); 
DebugOffRamp.prototype.constructor = DebugOffRamp;

DebugOffRamp.prototype.send = function(message) {
			
			if (!this.stopped) {
				util.log('DebugOffRamp -> sending message...');
				
				util.log('DebugOffRamp ->');	
				console.log(message.payload);
			}
		};

module.exports = DebugOffRamp;