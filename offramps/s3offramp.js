var util = require('util');

/*
 * Amazon S3 off ramp allows us to send messages to an S3 bucket.
 * 
 */
var OffRamp = require('./offramp');
var AWS = require('aws-sdk');

function S3OffRamp(){	
	this.bucketName = '';
	this.s3 = null;
	this.s3Parameters = null;
}

S3OffRamp.prototype = new OffRamp(); 
S3OffRamp.prototype.constructor = S3OffRamp;

S3OffRamp.prototype.provider = function(awsProvider) {
			this.s3 = awsProvider.getClient();
			return this;
		};
		
S3OffRamp.prototype.credentials = function(key, secret, region) {
			
			AWS.config.update({ 
				"accessKeyId": key, 
				"secretAccessKey": secret, 
				"region": region });

			this.s3 = AWS.s3();

			return this;
		};
		
S3OffRamp.prototype.bucket = function(bucketName) {
			this.bucketName = bucketName;
			return this;
		};

S3OffRamp.prototype.parameters = function(s3Parameters) {
			this.s3Parameters = s3Parameters;
			return this;
		};

		
S3OffRamp.prototype.send = function(message) {
	
				var params = this.s3Parameters || { ACL: 'private' };				

				if (!this.stopped) {
					util.log('S3 settings -> ');
					util.log(this.s3);

					var body = null;

					if (message.payload && message.payload.params) {
						for(var item in message.payload.params) {
							params[item] = message.payload.params[item];
						}
						body = this.toJSON(message.payload.data);
					} else {
						body = this.toJSON(message.payload);
					}

					params.Bucket = this.bucketName;
					params.Body = body;
					

					util.log('S3OffRamp -> sending message...');
					
					callback = function(err, response) {
						
						util.log('S3OffRamp -> response received');
						
						if (err) {
							util.log('ERROR -> ' + err);						
						}
						
						if (response) {
							util.log(response);
						}
										
					};
					
					console.log(message.payload);
					
					this.s3.putObject(params, data, callback);
				
				}
		};

module.exports = S3OffRamp;

