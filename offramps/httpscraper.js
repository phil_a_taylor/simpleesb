var util = require('util');
var async = require('async');
/*
 * HTTP Scraper Off Ramp allows us to scrape a web page and process the response messages.
 */

var OffRamp = require('./offramp');
var http = require('http');
var url = require('url');

function HttpScraper() {	
	this.destinationUrl = '';
	this.httpMethod = 'GET';
	this.scrapeHandler = null;	
	this.urlField = '';
	this.mergeResponse = false; // merge response with original source message
}

HttpScraper.prototype = new OffRamp(); 
HttpScraper.prototype.constructor = HttpScraper;

HttpScraper.prototype.Destination = function(address) {
			this.destinationUrl = address;
			return this;
		};

HttpScraper.prototype.HttpMethod = function(value) {
			this.httpMethod = value;
			return this;
		};

HttpScraper.prototype.ScrapeHandler = function(method) {
			this.scrapeHandler = method;
			return this;
		};

HttpScraper.prototype.UrlField = function(field) {
	this.urlField = field;
	return this;
};

HttpScraper.prototype.MergeResponse = function() {
	this.mergeResponse = true;
	return this;
};




HttpScraper.prototype.scrape = function(body, callback) {

	var jsdom = require('jsdom');

	var scraper = this;

	jsdom.env({
                html: body,
                scripts: ['http://code.jquery.com/jquery-1.6.min.js'],
                done : function(err, window){
    			
	                scraper.scrapeHandler(window.jQuery, function(err, messages){

							if (err) {
								callback(err);
							} else {
								callback(null, messages);
							}

						});
                
                }                
        	});

	};

HttpScraper.prototype.send = function(message) {
			
			var offramp = this;

			if (!this.stopped) {		


				if (this.urlField) {
					this.destinationUrl = message.payload[this.urlField];
				}

				util.log("HttpScraper -> sending message to: " + this.destinationUrl);
				
				var urlInfo = url.parse(this.destinationUrl);
				var method = this.httpMethod;

				var options = {
						  host: urlInfo.hostname,
						  path: urlInfo.path,
						  port: urlInfo.port,
						  auth: urlInfo.auth,
						  method: method
						};
				
				
				http_callback = function(response) {
							
						  util.log('HttpScraper -> STATUS (' + response.statusCode + ')');
					
						  response.on('error', function(e) {
							  util.log('HttpScraper - > problem with response: ' + e.message);						  
						  });
					
						  var str = '';
						  response.on('data', function (chunk) {
						    str += chunk;
						  });
	
						  response.on('end', function () {
						    

						    // util.log(str);

						    if (offramp.routeResponse) {
						    	util.log('routing response....');
						    	
						    	offramp.scrape(str, function(err, messages){

						    		if (err) {
						    			util.log('HttpScraper - > unknown error during scrapping: ' + err);
						    		} else {						    			

						    			util.log('scraped messages...');
						    			console.log(messages);

						    			if (messages instanceof Array) {
						    					
						    				async.each(messages, function(item, done){

						    					offramp.publish(item);
						    					done();

						    				}, 
						    				function(err){

						    					util.log('HttpScraper - > error processing scrapped messages: ' + err);	

						    				});

						    			} else {
						    				if (offramp.mergeResponse) {

						    					for(var prop in messages) {
						    						message.payload[prop] = messages[prop];						    						
						    					}

												offramp.publish(message.payload);						    					

							    			} else {

							    				offramp.publish(messages);
							    			}							    			
						    			}						    			
						    		}
						    	});
						    }
						  });
				};
				
				var request = http.request(options, http_callback);

				request.on('error', function(e) {
							  util.log('HttpScraper - > problem with request: ' + e.message);						  
						  });

				util.log(message.payload);
				
				request.end();
			}
		};

module.exports = HttpScraper;