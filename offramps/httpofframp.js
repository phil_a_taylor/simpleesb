var util = require('util');

/*
 * HTTP Off Ramp allows us to send messages to an http endpoint.
 * remarks: currently only supports JSON object payloads
 */

var OffRamp = require('./offramp');
var http = require('http');
var url = require('url');

function HttpOffRamp() {
	this.destinationUrl = '';
	this.httpMethod = 'GET';
	this.mergeResponse = false;
	this.urlField = '';
	this.messageField = '';
}

HttpOffRamp.prototype = new OffRamp();
HttpOffRamp.prototype.constructor = HttpOffRamp;

HttpOffRamp.prototype.Destination = function(address) {
			this.destinationUrl = address;
			return this;
		};

HttpOffRamp.prototype.HttpMethod = function(value) {
			this.httpMethod = value;
			return this;
		};

HttpOffRamp.prototype.MergeResponse = function() {
	this.mergeResponse = true;
	return this;
};

HttpOffRamp.prototype.UrlField = function(field) {
	this.urlField = field;
	return this;
};

HttpOffRamp.prototype.MessageField = function(field) {
	this.messageField = field;
	return this;
};

HttpOffRamp.prototype.send = function(message) {
			
			var offramp = this;

			if (!this.stopped) {	

				if (this.urlField) {
					this.destinationUrl = message.payload[this.urlField];
				}

				util.log("HttpOffRamp -> sending message to: " + this.destinationUrl);
				
				var urlInfo = url.parse(this.destinationUrl);
				var method = this.httpMethod;

				var data = (this.messageField) ? this.toJSON(message.payload[this.messageField]) : this.toJSON(message.payload);
				var msgLength = data.length;

				var options = {
						  host: urlInfo.hostname,
						  path: urlInfo.path,
						  port: urlInfo.port,
						  auth: urlInfo.auth,
						  method: method,
						  headers: {
						  	'Content-Type': 'application/json',
						  	'Content-Length':  Buffer.byteLength(data)
						  }
						};
				
				
				http_callback = function(response) {
							
						  util.log('HttpOffRamp -> STATUS (' + response.statusCode + ')');
					
						  response.on('error', function(e) {
							  util.log('HttpOffRamp - > problem with response: ');
							  console.log(e);
							  util.log('HttpOffRamp - > original message: '); 
							  console.log(message);
						  });
					
						  var str = '';
						  response.on('data', function (chunk) {
						    str += chunk;
						  });
	
						  response.on('end', function () {
						    


						    util.log(str);

						    if (response.statusCode == 200) {

							    if (offramp.routeResponse) {
							    	util.log('routing response....');
							    	
							    	var responseObj = JSON.parse(str);

							    	if (offramp.mergeResponse) {
							    		var newMsg = (typeof message.payload == 'object') ? message.payload : JSON.parse(message.payload);

							    		util.log('Mering response object...');
				    					for(var prop in responseObj) {
				    						newMsg[prop] = responseObj[prop];						    						
				    						util.log('Added \"' + prop + '\" to original message payload');
				    					}

										offramp.publish(newMsg);						    					

					    			} else {

					    				offramp.publish(responseObj);
					    			}		
							    
							    }		
							} else {

								util.log('HttpOffRamp - > problem with response ');							  
							  	util.log('HttpOffRamp - > original message: '); 
							  	console.log(message);
							}				    

						  });
				};
				
				var request = http.request(options, http_callback);

				request.on('error', function(e) {
							  util.log('HttpOffRamp - > problem with request: ' + e.message);						  
						  });

				console.log(data);
				request.write(data, 'utf-8');	
				request.end();
			}
		};

module.exports = HttpOffRamp;