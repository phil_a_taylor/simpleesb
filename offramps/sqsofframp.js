var util = require('util');

/*
 * Amazon SQS on ramp allows us to receive messages from an SQS message queue.
 * 
 */
var OffRamp = require('./offramp');
var SQS = require('aws-sqs');

function SqsOffRamp(){	
	this.queueName = '';
	this.sqs = null;
}

SqsOffRamp.prototype = new OffRamp(); 
SqsOffRamp.prototype.constructor = SqsOffRamp;

SqsOffRamp.prototype.provider = function(awsProvider) {
			this.sqs = awsProvider.getClient();
			return this;
		};
		
SqsOffRamp.prototype.credentials = function(key, secret, region) {
			this.sqs = new SQS(key, secret, {region: region });
			return this;
		};
		
SqsOffRamp.prototype.queue = function(queueName) {
			this.queueName = queueName;
			return this;
		};
		
SqsOffRamp.prototype.send = function(message) {
	
				if (!this.stopped) {
					util.log('SQS settings -> ');
					util.log(this.sqs);

					util.log('SqsOffRamp -> sending message...');
					
					sqs_callback = function(err, response) {
						
						util.log('SqsOffRamp -> response received');
						
						if (err) {
							util.log('ERROR -> ' + err);						
						}
						
						if (response) {
							util.log(response);
						}
										
					};
					
					console.log(message.payload);

					var data = this.toJSON(message.payload);
					
					this.sqs.sendMessage(this.queueName, data, sqs_callback);
				
				}
		};

module.exports = SqsOffRamp;

