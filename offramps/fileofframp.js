var util = require('util');
var fs = require('fs');

/*
 * Amazon SQS on ramp allows us to receive messages from an SQS message queue.
 * 
 */
var OffRamp = require('./offramp');

function FileOffRamp(){	
	this.folder = '';
	this.filename = '';

	this.createRandomFilename = function() {

    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
    
  };
}

FileOffRamp.prototype = new OffRamp(); 
FileOffRamp.prototype.constructor = FileOffRamp;

		
FileOffRamp.prototype.Folder = function(folderPath) {
			this.folder = folderPath;
			return this;
		};

FileOffRamp.prototype.Filename = function(filename) {
			this.filename = filename;
			return this;
		};

		
FileOffRamp.prototype.send = function(message) {
	
				if (!this.stopped) {
					
					var filename = (this.filename === '') ? this.createRandomFilename() : this.filename;

					util.log('FileOffRamp settings -> ');
					util.log('folder - ' + this.folder);
					util.log('filename - ' + filename);


					util.log('FileOffRamp -> sending message...');

					
					console.log(message.payload);

					var data = this.toJSON(message.payload);

					fs.writeFile(this.folder + '/' + filename, data, function(err){
						if (err) {
							util.log('FileOffRamp -> ERROR ' + err);						
						} else {
							util.log('File write successful.')
						}
					});
				
				}
		};

module.exports = FileOffRamp;

