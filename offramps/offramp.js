var util = require('util');

function OffRamp(){	
	this.bus = null;
	this.stopped = true;
	this.eventName = '';
	this.routeResponse = false;
	this.publicationEventName = null; // used with route response

	this.toJSON = function(obj) {
		if (typeof obj == 'object') {
			return JSON.stringify(obj);
		} else {
			return obj;
		}
	}
}

OffRamp.prototype = {
		
		configure: function(serviceBus) {			
			this.bus = serviceBus;
			return this;
		},
		
		subscribeTo: function(busEvent) {
			this.eventName = busEvent;
			return this;
		},
		
		publishAs: function(busEvent) {
			this.publicationEventName = busEvent;
			this.routeResponse = true;
			return this;
		},

		RouteResponse: function() {
			this.routeResponse = true;
			return this;
		},

		start : function() {
			
			var exp = new RegExp('^'.concat(this.eventName.replace('*', '.*'), '$'));
			
			var subscription = function(msg) {
				
				return msg.event.match(exp);
			};
			
			util.log('OffRamp -> start listening [' + this.eventName + ']...');
			this.stopped = false;
			
			
			var self = this;
			
			this.bus.subscribe(
					subscription, 
					function(msg) { self.send(msg); });
		},
		
		stop : function() {
			util.log('OffRamp -> shutting down...');
			this.stopped = true;
		},
		
		send: function(message){},

		publish : function(msg){
			
			util.log('OffRamp -> publish');
			var evt = this.publicationEventName;
			
			var busMsg = { event: evt, payload: msg };
			util.log('OffRamp -> publishing message...');
			console.log(busMsg);
			this.bus.post(busMsg);
		}		
};

module.exports = OffRamp;

