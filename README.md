# SimpleESB
-------------

A simple ESB application that can be used for Application Integration.  This ESB is simple and uses the concept(s) of an itineray which contains on-ramp(s), transform(s), off-ramp(s) and message events.  An on-ramp is an entry point onto the bus, likewise an off-ramp is an exit point off of the bus.  A transformation is the reorganization of the message that was received on the bus into a new format.  Finally, a message event is used to link the itinerary artifacts together (on-ramp, off-ramp, transform).  The following features are currently supported.

## Features
* On-Ramps
	+ File
	+ Excel
	+ Amazon SQS
* Off-Ramps
	+ Debug
	+ Http
	+ Amazon SQS
* Transformations
	+ JSON-to-JSON

## Limitations
Currently the ESB is only designed to work with JSON data. All messages must be in a JSON format when they leave the on-ramp into the bus.

## Sample Overview
The sample illustrates using the SimpleESB to transfer orders from a front end e-commerce website to a backend order fulfillment system.

![Simple ESB Diagram](https://bitbucket.org/phil_a_taylor/simpleesb/raw/master/diagram.png)


## Sample Itinerary
```javascript
var SimpleESB = require('simpleesb');

var yourmap = require('../transforms/yourmap');

exports.itinerary = new SimpleESB.Itinerary()
	.OnRamp(
		new SimpleESB.OnRamps.SqsOnRamp()						
			.publishAs('onramp::test')
			.provider(SimpleESB.SqsClientProvider.autoConfigure())
			.queue('/054218599934/Test'))
	
	.Transform(
		new SimpleESB.TransformService()			
			.provider(yourmap.createMap())
			.subscribeTo('onramp::test')
			.publishAs('transformed::test'))	

	.OffRamp(
		new SimpleESB.OffRamps.HttpOffRamp()
			.subscribeTo('transformed::test')
			.HttpMethod('POST')
			.Destination('http://localhost:8080/test/publish')
		);
```

## Sample Map
```javascript
var SimpleESB = require('simpleesb');

var yourmap = function() {
	
	return new SimpleESB.JsonMap()
		.field('*', 'status', function(source){ return 'PENDING'; })
		.field('created_at', 'createdOn')
		.field('payment.po_number', 'masterOrderNumber')
		.field('shipping_address.street', 'shipAddress')
		.field('shipping_address.city', 'shipCity')
		.field('shipping_address.region', 'shipState')
		.field('shipping_address.postcode', 'shipPostalCode')
		.field('*', 'shipTo', function(source) { return source.shipping_address.firstname + ' ' + source.shipping_address.lastname; })
		.field('customer_email', 'shipEmail')
		.field('shipping_address.telephone', 'shipPhone')
		.field('', 'shipMethod')
		.field('customer_email', 'userEmail')
		.field('', 'facilityName')		
		
		.collection('items', 'details', 
				new SimpleESB.JsonMap()
					.field('discount_amount', 'discount')
					.field('row_total', 'lineTotal')
					.field('qty_ordered', 'quantity')
					.field('price', 'salePrice')
					.field('tax_amount', 'salesTax')
					.field('price', 'unitPrice')
					.field('sku', 'sku')
					);
};

module.exports.createMap = yourmap;
```

**Notes:** *When mapping you typically supply a source field and a destination field. However, as you can see above you are also able to add a 3rd argument to specify a Javascript function to use when mapping the specified field. If you are using a Javascript function then you must specify an * in the source field.  When mapping a collection you will use the keyword collection and supply another map to use on the collection.*

# Running

To run the Simple ESB you can simply add the following to your application startup routine.

```javascript
	require('simpleesb').start();	
```
