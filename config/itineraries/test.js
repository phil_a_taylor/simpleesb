var Itinerary = require('../../common/itinerary');
var SqsOnRamp = require('../../onramps/sqsonramp');
var HttpOffRamp = require('../../offramps/httpofframp');
var TransformService = require('../../common/transformservice');
var sqsProvider = require('../../common/sqsclientprovider').autoConfigure();

var orderToBackOfficeMap = require('../transforms/ordertobackoffice');

exports.itinerary = new Itinerary()
	.OnRamp(
		new SqsOnRamp()						
			.publishAs('onramp::order')
			.provider(sqsProvider)
			.queue('/054218531534/Orders'))
	
	.Transform(
		new TransformService()			
			.provider(orderToBackOfficeMap.createMap())
			.subscribeTo('onramp::order')
			.publishAs('transformed::order'))	

	.OffRamp(
		new HttpOffRamp()
			.subscribeTo('transformed::order')
			.HttpMethod('POST')
			.Destination('http://localhost:8080/orders/publish')
		);
