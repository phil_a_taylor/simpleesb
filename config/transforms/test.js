var JsonMap = require('../../common/jsonmap');

var orderToBackOfficeMap = function() {
	
	return new JsonMap()
		.field('*', 'status', function(source){ return 'PENDING'; })
		.field('created_at', 'createdOn')
		.field('payment.po_number', 'masterOrderNumber')
		.field('shipping_address.street', 'shipAddress')
		.field('shipping_address.city', 'shipCity')
		.field('shipping_address.region', 'shipState')
		.field('shipping_address.postcode', 'shipPostalCode')
		.field('*', 'shipTo', function(source) { return source.shipping_address.firstname + ' ' + source.shipping_address.lastname; })
		.field('customer_email', 'shipEmail')
		.field('shipping_address.telephone', 'shipPhone')
		.field('', 'shipMethod')
		.field('customer_email', 'userEmail')
		.field('', 'facilityName')		
		
		.collection('items', 'details', 
				new JsonMap()
					.field('discount_amount', 'discount')
					.field('row_total', 'lineTotal')
					.field('qty_ordered', 'quantity')
					.field('price', 'salePrice')
					.field('tax_amount', 'salesTax')
					.field('price', 'unitPrice')
					.field('sku', 'sku')
					);
};

module.exports.createMap = orderToBackOfficeMap;