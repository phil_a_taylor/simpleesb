/*
 * Excel based onramp allows us to watch a file location and pickup files for processing.
 */
var FileOnRamp = require('./fileonramp');
var excel = require('excel');
var async = require('async');

function ExcelOnRamp(){
	
	//FileOnRamp.call(this);
	//
	this.split = false;
	this.native = false;
	this.createJSONObject = function(headerArray, bodyArray) {

		if (headerArray == null || bodyArray == null) throw Error('Cannot create object from null array!');
		if (headerArray.length != bodyArray.length) throw Error('Cannot create object from arrays of two different sizes!');

		var obj = {};
		for(var i = 0; i < headerArray.length; i++) {
			obj[headerArray[i]] = bodyArray[i];
		}

		return obj;
	}
}

ExcelOnRamp.prototype = new FileOnRamp(); 

ExcelOnRamp.prototype.constructor = ExcelOnRamp;

ExcelOnRamp.prototype.Split = function() {
	this.split = true;
	return this;
}

ExcelOnRamp.prototype.Native = function() {
	this.native = true;
	return this;
}

ExcelOnRamp.prototype.process = function(file, callback) {
	
	//TODO: Add map to translate array of arrays to something like the following
	//
	// { filename: "sample.xlsx",
	// 	  data: [
	// 	  	{ columnA: "abc", columnB: "def", columnC: "xyz" },
	// 	  	{ columnA: "123", columnB: "456", columnC: "789" }
	// 	  ]
	// }
	//
	var self = this,
		native = this.native,
		split = this.split,
		filename = file.slice(file.search(/[^\/]*$/)).replace('.processing', '');

	excel(file, function(err, data){

		if (err) callback(err);

		if (split) {

			if (native) {

				async.each(data, function(item, done){
					var output = {};
					output.filename = filename;
					output.data = item;
					self.publish(output);
					done();
				},
				function (err) {
					if (err) callback(err);
					
					// already published return nothing
					callback(null, null); 
				});
			} else {
				var header = data.shift();	
				async.each(data, function(item, done){
					var output = {};
					output.filename = filename;
					output.data = self.createJSONObject(header, item);
					self.publish(output);
					done();
				},
				function (err) {
					if (err) callback(err);

					// already published return nothing
					callback(null, null);
				});	
			}

		} else {
			var output = {};
			output.filename = filename;
			output.data = [];

			if (native) {
				output.data = data;
				callback(null, output);
			} else {
				var header = data.shift();	
				async.each(data, function(item, done){

					var obj = self.createJSONObject(header, item);
					output.data.push(obj);
					done();
				},
				function (err) {
					if (err) callback(err);
					callback(null, output);
				});
			}

		}
	});
}

module.exports = ExcelOnRamp;

