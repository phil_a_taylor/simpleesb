/*
 * File based onramp allows us to watch a file location and pickup files for processing.
 */
var OnRamp = require('./onramp');
var fs = require('fs');
var async = require('async');
var util = require('util');

function FileOnRamp(){
		
	this.location = null;
	this.filter = "*.*";
}

FileOnRamp.prototype = new OnRamp(); 

FileOnRamp.prototype.constructor = FileOnRamp;

FileOnRamp.prototype.Location = function(location) {
	this.location = location.match(/.*\/$/) ? location : location + '/';
	return this;	
}

FileOnRamp.prototype.Filter = function(filter) {
	this.filter = filter;
	return this;
}

FileOnRamp.prototype.scan = function() {
	
	if (!this.stopped){

		var self = this;
		var location = this.location;
		var filter = this.filter;
		var filterExp = new RegExp(filter.replace(/\./g, '\.').replace(/\*/g,'\.*') + '$');
		
		
		util.log('FileOnRamp -> ' + location + filter);		
		

		fs.readdir(location, function(err, files){
			if (err){
				util.log('Error monitoring location: ' + location );
				util.error(err);	
			} else {
				if (files && files.length > 0) {
					var filtered = files.filter(function(item){
						return item.match(filterExp)
					});

					if (filtered && filtered.length > 0) {
						
						util.log('...processing files(s)')

						async.each(filtered, function(file, done){
							var path = location + '/' + file;
							
							self.pickup(path, function(err, data){						
									if (err) {
										util.log('Error reading file: ' + path);
										util.error(err);	
										done(err);
									} else {
										if (data) self.publish(data.toString());	
										done(null);
								}
							});
									
						},
						function(err){
							util.log('...finished processing file(s)')
							if (err) {
								util.log('Error processing: ' + location);
								util.error(err);	
							}

							if (!this.stopped) {
								util.log('...will check again in 15 seconds')
								setTimeout(self.scan.bind(self), 15*1000);
							}
						});
						
					} else {
						util.log('FileOnRamp -> no files');
						if (!this.stopped) {
							util.log('...will check again in 15 seconds')
							setTimeout(self.scan.bind(self), 15*1000);
						}	
					}

				} else {
					util.log('FileOnRamp -> no files');
					if (!this.stopped) {
						util.log('...will check again in 15 seconds')
						setTimeout(self.scan.bind(self), 15*1000);
					}
				}
				
			}
		});
	}
}

FileOnRamp.prototype.pickup = function(file, callback) {
	var self = this;
	var pFile = file + '.processing';

	util.log('FileOnRamp -> receiving message...');

	async.waterfall([
			function(callback){
				fs.rename(file, pFile, callback);
			},
			function(callback) {			
				util.log('FileOnRamp -> processing: ' + file);	
				self.process(pFile, callback);
			},
			function(data, callback){
				fs.unlink(pFile, function(err){
					if (err) callback(err);
					callback(null, data);
				});
			}
		],
		function(err, data) {
			if (err) {
				callback(err);
			} else {
				callback(null, data);
			}	
		});
}

FileOnRamp.prototype.process = function(file, callback) {	
	fs.readFile(file, callback);
}
		
FileOnRamp.prototype.listen = function(callback) {
			if (!this.stopped) {
				
				var self = this;
				
				setTimeout(self.scan.bind(self), 5*1000);			
			}
		};

module.exports = FileOnRamp;

