var util = require('util');

/*
 * OnRamp is the base class for all on ramps. An on ramp has the following 
 * behavior by definition. 
 * 	(1) Listen for messages
 *  (2) Publish message to the bus with supplied event information
 *  
 *  Bus Message Format
 *  {
 *  	event: 'onramp::yourtype',
 *  	payload: 
 *  		{
 *  			title: 'the real message will replace this JSON object'
 *  		}
 *  }
 *  
 */

function OnRamp() {
	this.bus = null;
	this.stopped = true;
	this.eventName = '';
}

OnRamp.prototype = {
		
		configure: function(serviceBus) {			
			this.bus = serviceBus;
			return this;
		},
		
		publishAs: function(busEvent) {
			this.eventName = busEvent;
			return this;
		},
		
		start : function() {
			util.log('OnRamp -> start listening [' + this.eventName + ']...');
			this.stopped = false;						
			this.listen();
		},
		
		stop : function() {
			util.log('OnRamp -> shutting down...');
			this.stopped = true;
		},
		
		publish : function(msg){
			
			util.log('OnRamp -> publish');
			var evt = this.eventName;
			
			var busMsg = { event: evt, payload: msg };
			util.log('OnRamp -> publishing message...');
			console.log(busMsg);
			this.bus.post(busMsg);
		},
		
		listen: function(callback){} // replace with your listener
};

module.exports = OnRamp;