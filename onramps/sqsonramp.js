/*
 * Amazon SQS on ramp allows us to receive messages from an SQS message queue.
 * 
 */
var OnRamp = require('./onramp');
var SQS = require('aws-sqs');
var util = require('util');

function SqsOnRamp(){	
	this.sqs = null;
}

SqsOnRamp.prototype = new OnRamp(); 

SqsOnRamp.prototype.constructor = SqsOnRamp;

SqsOnRamp.prototype.provider = function(sqsProvider) {
			this.sqs = sqsProvider.getClient();
			return this;
		};
		
SqsOnRamp.prototype.credentials = function(key, secret, region) {
			
			this.sqs = new SQS(key, secret, {region: region });
			return this;
		};
		
SqsOnRamp.prototype.queue = function(queueName) {
			this.queueName = queueName;
			return this;
		};
		
SqsOnRamp.prototype.listen = function(callback) {
			if (!this.stopped) {
				
				
				var self = this;
				
				sqs_callback = function(err,response) {					
					
					if (err) {
						util.log('ERROR -> ' + err);						
						return;
					}
					
					if (response) {
						util.log('SqsOnRamp -> receiving message...');
						util.log(response);
						
						var msg = response[0];
						
						util.log('SqsOnRamp -> deleting message: ' + msg.MessageId);
						
						self.sqs.deleteMessage(self.queueName,  msg.ReceiptHandle, function(e){
							
							if (e) {
							
								util.log('ERROR -> ' + e);
								return;
							}
						});
						
						self.publish(msg.Body);
						
					}
					
					self.listen(callback);
					
				};
				
				this.sqs.receiveMessage(this.queueName, { MaxNumberOfMessages: 1, VisibilityTimeout: 30}, sqs_callback);
			
			}
		};

module.exports = SqsOnRamp;

